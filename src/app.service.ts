import { Injectable,Req } from '@nestjs/common';
import { Request } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JuapiServiceLogEntity } from 'src/entities/juapiservicelog.entity';

@Injectable()
    export class AppService {   
    constructor(
        @InjectRepository(JuapiServiceLogEntity)
        private apiServiceLog: Repository<JuapiServiceLogEntity>,

    ) {}

  
    async saveApiServiceLog(@Req() req) {
        const result    = await this.apiServiceLog.create();
        const resultLog = await this.apiServiceLog.save(req);
        // console.log(resultLog);
      }
}