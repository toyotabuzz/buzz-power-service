import { Column, Entity, Index, PrimaryGeneratedColumn,BeforeInsert } from "typeorm";

@Index("idx_juapi_service_log_method_name", ["method_name"], {})
@Index("idx_juapi_service_log_in_service", ["in_service"], {})
@Entity("juapi_service_log")
export class JuapiServiceLogEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("varchar", { name: "method_name", nullable: true, length: 150 })
  method_name: string | null;

  @Column("text", { name: "in_service", nullable: true })
  in_service: string | null;

  @Column("text", { name: "return_service", nullable: true })
  return_service: string | null;

  @Column("datetime", { name: "created_at", nullable: true })
  created_at: Date | null;

  @Column("varchar", { name: "created_by", nullable: true, length: 20 })
  created_by: string | null;

  @Column("datetime", { name: "updated_at", nullable: true })
  updated_at: Date | null;

  @Column("varchar", { name: "updated_by", nullable: true, length: 20 })
  updated_by: string | null;
}
