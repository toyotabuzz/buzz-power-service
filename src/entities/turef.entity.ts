import { Entity, Column, PrimaryGeneratedColumn, Index, OneToMany, OneToOne, JoinColumn } from 'typeorm';

import * as helpers from 'src/Helpers/helpers';

@Entity({ name: 'turef' })
export class TurefEntity {
  @Index('idx_ref_show')
  @Index('idx_ref_search')
  @Index('idx_ref_code')
  @Index('idx_ref_desc')
  @Index('idx_ref_type')
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 130,
  })
  ref_type: string;

  @Column({
    type: 'varchar',
    length: 130,
  })
  ref_code: string;

  @Column({
    type: 'int',
  })
  ref_seq: number;

  @Column({
    type: 'varchar',
    length: 150,
  })
  ref_desc: string;

  @Column({
    type: 'varchar',
    length: 20,
  })
  ref_value: string;

  @Column({
    type: 'text',
  })
  ref_remark: string;

  @Column({
    type: 'char',
    length: 1,
  })
  isactive: string;

  @Column({
    type: 'datetime',
  })
  created_at: string;

  @Column({
    type: 'int',
  })
  created_by: number;

  @Column({
    type: 'datetime',
  })
  updated_at: string;

  @Column({
    type: 'int',
  })
  updated_by: number;
}