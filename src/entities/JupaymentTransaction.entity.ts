import { 
  Column, 
  Entity, 
  Index, 
  PrimaryGeneratedColumn
} from "typeorm";

@Index("idx_payment_id", ["payment_id"], {})
@Entity("jupayment_transaction")
export class JupaymentTransactionEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "payment_id", nullable: true })
  payment_id: number | null;

  @Column("int", { name: "pay_type", nullable: true })
  pay_type: number | null;

  @Column("varchar", { name: "brand", nullable: true, length: 20 })
  brand: string | null;

  @Column("varchar", { name: "card_number", nullable: true, length: 16 })
  card_number: string | null;

  @Column("varchar", { name: "card_bank", nullable: true, length: 100 })
  card_bank: string | null;

  @Column("varchar", { name: "order_id", nullable: true, length: 100 })
  order_id: string | null;

  @Column("varchar", { name: "pay_code", nullable: true, length: 100 })
  pay_code: string | null;

  @Column("varchar", { name: "pay_code_desc", nullable: true, length: 200 })
  pay_code_desc: string | null;

  @Column("int", { name: "pay_round", nullable: true })
  pay_round: number | null;

  @Column("datetime", { name: "pay_date", nullable: true })
  pay_date: Date | null;

  @Column("varchar", { name: "transaction_status", nullable: true, length: 20 })
  transaction_status: string | null;

  @Column("varchar", { name: "pay_status", nullable: true, length: 100 })
  pay_status: string | null;

  @Column("varchar", { name: "reference_order", nullable: true, length: 200 })
  reference_order: string | null;

  @Column("varchar", { name: "chrg_test", nullable: true, length: 200 })
  chrg_test: string | null;

  @Column("text", { name: "notify_verify", nullable: true })
  notify_verify: string | null;

  @Column("varchar", { name: "source_id", nullable: true, length: 200 })
  source_id: string | null;

  @Column("datetime", { name: "created_at", nullable: true })
  created_at: Date | null;

  @Column("varchar", { name: "created_by", nullable: true, length: 20 })
  created_by: string | null;

  @Column("datetime", { name: "updated_at", nullable: true })
  updated_at: Date | null;

  @Column("varchar", { name: "updated_by", nullable: true, length: 20 })
  updated_by: string | null;

}
