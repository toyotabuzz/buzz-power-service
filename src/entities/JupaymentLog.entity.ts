import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index("idx_code_check", ["code_check"], {})
@Index("idx_ref_no", ["reference_order"], {})
@Index("idx_customer_tel", ["customer_tel"], {})
@Index("idx_jupayment_id", ["jupayment_id"], {})
@Entity("jupayment_log")
export class JupaymentLogEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "id" })
  id: number;

  @Column("int", { name: "jupayment_id", nullable: true })
  jupayment_id: number | null;

  @Column("varchar", {
    name: "code_check",
    nullable: true,
    comment: "รหัสจ่ายเงิน",
    length: 255,
  })
  code_check: string | null;

  @Column("varchar", { name: "method", comment: "ชื่อ method", length: 255 })
  method: string;

  @Column("int", { name: "ref_id", nullable: true })
  ref_id: number | null;

  @Column("varchar", { name: "reference_order", length: 50 })
  reference_order: string;

  @Column("double", { name: "amount", nullable: true, precision: 10, scale: 2 })
  amount: number | null;

  @Column("varchar", { name: "customer_name", nullable: true, length: 255 })
  customer_name: string | null;

  @Column("varchar", { name: "customer_tel", nullable: true, length: 10 })
  customer_tel: string | null;

  @Column("varchar", { name: "merchant_id", nullable: true, length: 100 })
  merchant_id: string | null;

  @Column("varchar", { name: "merchant_name", nullable: true, length: 200 })
  merchant_name: string | null;

  @Column("char", { name: "pay_type", nullable: true, length: 1 })
  pay_type: string | null;

  @Column("varchar", { name: "text_sms", nullable: true, length: 500 })
  text_sms: string | null;

  @Column("datetime", { name: "send_sms_at", nullable: true })
  send_sms_at: Date | null;

  @Column("text", { name: "bitly_url", nullable: true })
  bitly_url: string | null;

  @Column("char", { name: "is_sms", nullable: true, length: 3 })
  is_sms: string | null;

  @Column("char", { name: "is_payment", nullable: true, length: 3 })
  is_payment: string | null;

  @Column("char", {
    name: "is_active",
    nullable: true,
    length: 1,
    default: () => "'Y'",
  })
  is_active: string | null;

  @Column("int", { name: "transaction_id", nullable: true })
  transaction_id: number | null;

  @Column("varchar", { name: "pay_status", nullable: true, length: 100 })
  pay_status: string | null;

  @Column("datetime", { name: "pay_date", nullable: true })
  pay_date: Date | null;

  @Column("datetime", { name: "created_at", nullable: true })
  created_at: Date | null;

  @Column("varchar", { name: "created_by", nullable: true, length: 20 })
  created_by: string | null;

  @Column("datetime", { name: "updated_at", nullable: true })
  updated_at: Date | null;

  @Column("varchar", { name: "updated_by", nullable: true, length: 20 })
  updated_by: string | null;
}
