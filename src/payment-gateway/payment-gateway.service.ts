import { HttpStatus, Injectable, Req } from '@nestjs/common';
import { query, Request } from 'express';
import { InjectRepository } from '@nestjs/typeorm';
import { JupaymentEntity } from 'src/entities/jupayment.entity';
import { JupaymentLogEntity } from 'src/entities/jupaymentlog.entity';
import { JupaymentTransactionEntity } from 'src/entities/jupaymenttransaction.entity';
import { TurefEntity } from 'src/entities/turef.entity';
import { createQueryBuilder, Repository, getConnection } from 'typeorm';
import { params } from 'src/config/config.params';
// import { PaymentGatewayExtranal } from 'src/dto/paymentGatewayExtranal.dto';
import * as Helpers from 'src/Helpers/helpers';

@Injectable()
export class PaymentGatewayService {
    constructor(
        @InjectRepository(JupaymentEntity)
        private jupayment: Repository<JupaymentEntity>,

        @InjectRepository(JupaymentLogEntity)
        private jupaymentlog: Repository<JupaymentLogEntity>,

        @InjectRepository(JupaymentTransactionEntity)
        private jupaymenttransaction: Repository<JupaymentTransactionEntity>,
    ) { }

    async getDataSendSms(@Req() request) {
        try {
            const { id, method, ref_id, amount, customer_name, customer_tel } =
                request;

            const query = await getConnection()
                .createQueryBuilder()
                .from(JupaymentEntity, 'jupayment');

            if (!Helpers.isEmpty(method)) {
                await query.andWhere('`jupayment`.`method` = :method', {
                    method: method,
                });
            }

            if (!Helpers.isEmpty(ref_id)) {
                await query.andWhere('`jupayment`.`ref_id` = :ref_id', {
                    ref_id: ref_id,
                });
            }

            if (!Helpers.isEmpty(amount)) {
                await query.andWhere('`jupayment`.`amount` = :amount', {
                    amount: amount,
                });
            }

            if (!Helpers.isEmpty(customer_name)) {
                await query.andWhere('`jupayment`.`customer_name` = :customer_name', {
                    customer_name: customer_name,
                });
            }

            if (!Helpers.isEmpty(customer_tel)) {
                await query.andWhere('`jupayment`.`customer_tel` = :customer_tel', {
                    customer_tel: customer_tel,
                });
            }

            await query.orderBy('`jupayment`.`id`', 'DESC');

            const result = await query.getRawOne();
            //await Helpers.getRawSql(query);

            return result;
        } catch (e) {
            return e.message;
        }
    }

    async checkDataPayment(@Req() request) {
        try {
            const { code_check, payment_id, ref_id } = request;

            const query = await getConnection()
                .createQueryBuilder()
                .from(JupaymentEntity, 'jupayment');

            if (!Helpers.isEmpty(code_check)) {
                let code_check_string = (<string>code_check).replace(/\s+/g, '+');
                await query.andWhere('`jupayment`.`code_check` = :code_check', {
                    code_check: code_check_string,
                });
            }
            if (!Helpers.isEmpty(payment_id)) {
                await query.andWhere('`jupayment`.`id` = :id', { id: payment_id });
            }
            if (!Helpers.isEmpty(ref_id)) {
                await query.andWhere('`jupayment`.`ref_id` = :ref_id', {
                    ref_id: ref_id,
                });
            }

            await query.orderBy('`jupayment`.`id`', 'DESC');

            const result = await query.getRawOne();
            //await Helpers.getRawSql(query);

            return result;
        } catch (e) {
            return e.message;
        }
    }

    async getDataPayment(@Req() request) {
        try {
            const { code_check, payment_id, ref_id, method } = request;

            const query = await getConnection()
                .createQueryBuilder()
                .select([
                    '`jupayment`.`id`',
                    '`jupayment`.`amount`',
                    '`jupayment`.`customer_name`',
                    '`jupayment`.`customer_tel`',
                    '`jupayment`.`merchant_id`',
                    '`jupayment`.`merchant_name`',
                    '`jupayment`.`text_sms`',
                    '`jupayment`.`bitly_url`',
                    '`jupayment`.`pay_status`',
                    '`jupayment`.`reference_order`',
                    '`jupayment`.`code_check`',
                    '`jupayment`.`method`',
                    '`jupayment`.`ref_id`',
                    'DATE_FORMAT(`jupayment`.`created_at`, "%Y-%m-%d %H:%i:%s") AS jupayment_created_at',
                    'DATE_FORMAT(`jupayment_transaction`.`pay_date`, "%Y-%m-%d %H:%i:%s") AS pay_date',
                    '`jupayment_transaction`.`payment_id`',
                    '`jupayment_transaction`.`pay_round`',
                    '`jupayment_transaction`.`brand`',
                    '`jupayment_transaction`.`card_number`',
                    '`jupayment_transaction`.`card_bank`',
                    '`jupayment_transaction`.`created_at`',
                    '`jupayment_transaction`.`chrg_test`',
                    'MAX(`jupayment_transaction`.`id`) AS `transaction_id_max`',
                    '`jupayment`.`transaction_id`',
                    '(SELECT `jupayment_transaction`.`pay_type`' +
                    'FROM `jupayment` `jupayment_sub`' +
                    'INNER JOIN `jupayment_transaction` ON `jupayment_transaction`.`payment_id` = `jupayment_sub`.`id`' +
                    'WHERE `jupayment`.`ref_id` = `jupayment_sub`.`ref_id`' +
                    'AND `jupayment`.`method` = `jupayment_sub`.`method`' +
                    'AND	`jupayment_transaction`.`pay_status` = "success"' +
                    ') AS `pay_type_id`',
                    '( SELECT `turef`.`ref_desc`' +
                    'FROM `jupayment` `jupayment_sub`' +
                    'INNER JOIN `jupayment_transaction` ON `jupayment_transaction`.`payment_id` = `jupayment_sub`.`id`' +
                    'INNER JOIN `turef` ON `turef`.`ref_value` = `jupayment_transaction`.`pay_type` AND `turef`.`ref_type` = "PAYMENT" AND `turef`.`ref_code` = "FORM_TYPE" AND `turef`.`isactive` = "Y" ' +
                    'WHERE `jupayment`.`ref_id` = `jupayment_sub`.`ref_id`' +
                    'AND `jupayment`.`method` = `jupayment_sub`.`method`' +
                    'AND `jupayment_transaction`.`pay_status` = "success"' +
                    ') AS `pay_type`',
                    '`jupayment_transaction`.`source_id`',
                    '`jupayment_transaction`.`pay_code`',
                    '`jupayment_transaction`.`pay_code_desc`',
                    '(SELECT `failure_message_th` FROM ' +
                    process.env.DB_DATABASE +
                    '.`tupayment_failure` WHERE `failure_code` = `jupayment_transaction`.`pay_code`) AS `pay_code_desc_th`',
                ])
                .from(JupaymentEntity, 'jupayment')
                .leftJoin(
                    JupaymentTransactionEntity,
                    'jupayment_transaction',
                    '`jupayment_transaction`.`payment_id` = `jupayment`.`id`',
                )
                .where("`jupayment`.`is_active` = 'Y'");

            if (!Helpers.isEmpty(code_check)) {
                await query.andWhere('`jupayment`.`code_check` = :code_check', {
                    code_check: code_check,
                });
            }
            if (!Helpers.isEmpty(payment_id)) {
                await query.andWhere('`jupayment`.`id` = :id', {
                    id: payment_id,
                });
            }
            if (!Helpers.isEmpty(ref_id)) {
                await query.andWhere('`jupayment`.`ref_id` = :ref_id', {
                    ref_id: ref_id,
                });
            }
            if (!Helpers.isEmpty(method)) {
                await query.andWhere('`jupayment`.`method` = :method', {
                    method: method,
                });
            }
            await query.groupBy('`jupayment`.`id`');
            // await Helpers.getRawSql(query);
            const result = await query.getRawOne();

            return result;
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                icons: '',
                message: e.message,
                results: {},
            };
        }
    }

    async getRoundMax(@Req() request) {
        try {
            const { payment_id } = request;

            const query = await getConnection()
                .createQueryBuilder()
                .select(['MAX(`jupayment_transaction`.`pay_round`) AS max_id'])
                .from(JupaymentTransactionEntity, 'jupayment_transaction')
                .where('`jupayment_transaction`.`payment_id` = :payment_id', {
                    payment_id: payment_id,
                })
                .groupBy('`jupayment_transaction`.`payment_id`');

            const result = await query.getRawOne();
            //await Helpers.getRawSql(query);

            return result;
        } catch (e) {
            return e.message;
        }
    }

    async getRefByValue(@Req() request) {
        try {
            const { ref_type, ref_code, ref_value, isactive } = request;

            const query = await getConnection()
                .createQueryBuilder()
                .select(['`turef`.`ref_value` AS id', '`turef`.`ref_desc` AS name'])
                .from(TurefEntity, 'turef')
                .where('`turef`.`isactive` = :isactive', { isactive: isactive })
                .andWhere('`turef`.`ref_type` = :ref_type', { ref_type: ref_type })
                .andWhere('`turef`.`ref_code` = :ref_code', { ref_code: ref_code })
                .andWhere('`turef`.`ref_value` = :ref_value', { ref_value: ref_value })
                .orderBy('`turef`.`ref_seq`', 'DESC');

            const result = await query.getRawOne();
            //await Helpers.getRawSql(query);

            return result;
        } catch (e) {
            return e.message;
        }
    }

    async createJupayment(@Req() request) {
        try {
            const jupayment = await this.jupayment.create(request);
            const result = this.jupayment.save(jupayment);

            return result;
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                response: e.message,
            };
        }
    }

    async insertJupaymentTransaction(@Req() request) {
        try {
            const jupaymenttransaction = await this.jupaymenttransaction.create(
                request,
            );
            const result = this.jupaymenttransaction.save(jupaymenttransaction);

            return result;
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                response: e.message,
            };
        }
    }

    async updateJupayment(@Req() request) {
        try {
            const queryUpdateJupayment = await createQueryBuilder()
                .update(JupaymentEntity)
                .set(request)
                .where('id = :id', { id: request.id })
                .execute();

            return queryUpdateJupayment.affected;
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                response: e.message,
            };
        }
    }

    async insertJupaymentLog(@Req() request) {
        try {
            const jupaymentlog = await this.jupaymentlog.create(request);
            const result = this.jupaymentlog.save(jupaymentlog);

            return result;
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                response: e.message,
            };
        }
    }

    async updatePaymentTransaction(@Req() request) {
        try {
            let {
                payment_id, //ส่งมา
                transaction_id, //ส่งมา
                reference_order,
                chrg_test,
                brand,
                card_number,
                card_bank,
                pay_code,
                pay_code_desc,
            } = request.query;
            //Update JupaymentTransaction
            //ถ้า transaction_id ไม่ว่าง
            // console.log(request.query);
            if (!Helpers.isEmpty(transaction_id)) {
                let paramTransaction = {
                    chrg_test: chrg_test,
                    reference_order: reference_order,
                    brand: brand,
                    card_number: card_number,
                    card_bank: card_bank,
                    pay_code: pay_code,
                    pay_code_desc: pay_code_desc,
                };

                const queryJupaymentTransaction = await createQueryBuilder()
                    .update(JupaymentTransactionEntity)
                    .set(paramTransaction)
                    .where('id = :id', { id: transaction_id })
                    .execute();
                // console.log(query);
                //ถ้า Update Transaction
                //Update Jupayment ด้วย payment_id
                if (!Helpers.isEmpty(payment_id)) {
                    let paramPayment = {
                        reference_order: reference_order,
                        pay_date: Helpers.dateNow(),
                        transaction_id: transaction_id,
                    };
                    const queryJupayment = await createQueryBuilder()
                        .update(JupaymentEntity)
                        .set(paramPayment)
                        .where('id = :id', { id: payment_id })
                        .execute();
                    // console.log(queryJupayment);

                    if (queryJupayment.affected) {
                        return {
                            statusCode: HttpStatus.OK,
                            success: true,
                            result: queryJupayment.affected,
                        };
                    }

                    if (queryJupaymentTransaction.affected) {
                        return {
                            statusCode: HttpStatus.OK,
                            success: true,
                            result: queryJupaymentTransaction.affected,
                        };
                    }
                }
            }
        } catch (e) {
            return {
                statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                success: false,
                response: e.message,
            };
        }
    }

    async updatePaymentTransactionStatus(@Req() request) {
        // console.log('message');
        let data = {
            statusCode: {},
            success: false,
            errors: '',
            results: {},
            data_transaction: {},
            data_payment: {},
        };
        try {
            let {
                chrg_test,
                reference_order,
                status,
                failure_code,
                failure_message,
                transaction_status,
                notify_verify,
                source_id,
            } = request.query;
            //ถ้า transaction_id ไม่ว่าง
            if (!Helpers.isEmpty(reference_order)) {
                //ดึงข้อมูลจาก JuPaymentTransaction

                let paramTransaction = {
                    source_id: source_id,
                    pay_status: status,
                    pay_code: failure_code,
                    pay_code_desc: failure_message,
                    transaction_status: transaction_status,
                    chrg_test: chrg_test,
                    notify_verify: notify_verify,
                    pay_date: Helpers.dateNow(),
                };
                //แก้ไขสถานะ JupaymentTransaction
                const queryUpdateJupaymentTransaction = await createQueryBuilder()
                    .update(JupaymentTransactionEntity)
                    .set(paramTransaction)
                    .where('reference_order = :reference_order', {
                        reference_order: reference_order,
                    })
                    .execute();
                const getDataPaymentTransactionByRef = await this.getDataPaymentTransactionByRef(request);
                // await Helpers.getRawSql(queryUpdateJupaymentTransaction);
                // console.log(getDataPaymentTransactionByRef);
                //แก้ไขสถานะ Jupayment
                let paramPayment = {
                    reference_order: reference_order,
                    pay_date: Helpers.dateNow(),
                    pay_type: getDataPaymentTransactionByRef.results.pay_type_id,
                    pay_status: status,
                    transaction_id: getDataPaymentTransactionByRef.results.id,
                };
                // console.log(paramPayment)
                const queryUpdateJupayment = await createQueryBuilder()
                    .update(JupaymentEntity)
                    .set(paramPayment)
                    .where('id = :id', {
                        id: getDataPaymentTransactionByRef.results.payment_id,
                    })
                    .execute();

                if (
                    queryUpdateJupaymentTransaction.affected &&
                    queryUpdateJupayment.affected
                ) {
                    //ดึงข้อมูล jupaymentTransaction ล่าสุด
                    const getResultPaymentTransaction =
                        await this.getDataPaymentTransactionByRef(request);
                    //ดึงข้อมูล jupayment ล่าสุด
                    const getResultPayment = await this.jupayment.findOne({
                        where: {
                            id: getResultPaymentTransaction.results.payment_id,
                        },
                    });

                    const getDataPaymentTransactionAfterUpdate =
                        await this.getDataPaymentTransactionByRef(request);

                    data.statusCode = HttpStatus.OK;
                    data.success = true;
                    data.results = getResultPaymentTransaction;
                    data.data_payment = getResultPayment;
                    data.data_transaction = getDataPaymentTransactionAfterUpdate;

                    return data;
                } else {
                    data.errors = 'Empty data select jupaymentTransaction!';
                }
            } else {
                data.errors = 'Reference_order, chrg_test empty!';
            }
        } catch (e) {
            return e.message;
        }
    }

    async getDataPaymentTransactionByRef(@Req() request) {
        let data = {
            statusCode: {},
            success: false,
            results: {},
            message: '',
        };
        try {
            let { chrg_test, reference_order } = request.query;
            // console.log(request.query);
            if (Helpers.isEmpty(reference_order) && Helpers.isEmpty(chrg_test)) {
                data.message = 'Field reference_order,chrg_test empty!';
            } else {
                const getDataPaymentTransaction = await this.findPaymentTransaction(
                    request,
                );

                data.statusCode = HttpStatus.OK;
                data.success = true;
                data.results = getDataPaymentTransaction;
                return data;
            }
        } catch (e) {
            return e.message;
        }
    }

    /**
     **************************************
     ******** JuTransactionSearch *********
     **************************************
     */

    async findPaymentTransaction(@Req() request: Request) {
        const { reference_order, chrg_test } = request.query;
        let data = {
            message: '',
        };
        try {
            const query = await getConnection()
                .createQueryBuilder()
                .select([
                    '`jupayment_transaction`.`id`',
                    '`jupayment_transaction`.`payment_id`',
                    '`jupayment`.`ref_id`',
                    '`jupayment`.`amount`',
                    '`jupayment`.`customer_name`',
                    '`jupayment`.`customer_tel`',
                    '`jupayment`.`merchant_id`',
                    '`jupayment`.`merchant_name`',
                    '`jupayment`.`text_sms`',
                    '`jupayment`.`bitly_url`',
                    'DATE_FORMAT(`jupayment_transaction`.`pay_date`, "%Y-%m-%d %H:%i:%s") AS pay_date',
                    '`jupayment_transaction`.`pay_round`',
                    '`jupayment_transaction`.`brand`',
                    '`jupayment_transaction`.`card_number`',
                    '`jupayment_transaction`.`card_bank`',
                    '`jupayment_transaction`.`pay_status`',
                    '`jupayment_transaction`.`created_at`',
                    '`jupayment_transaction`.`reference_order`',
                    '`jupayment_transaction`.`chrg_test`',
                    '`jupayment_transaction`.`notify_verify`',
                    '`jupayment_transaction`.`pay_code`',
                    '`jupayment_transaction`.`pay_code_desc`',
                    '`jupayment_transaction`.`pay_type` AS `pay_type_id`',
                    '(SELECT `ref_desc` FROM ' +
                    process.env.DB_DATABASE +
                    '.`turef` WHERE `ref_type` = "PAYMENT" AND `ref_code` = "FORM_TYPE" AND `ref_value` = `jupayment_transaction`.`pay_type` AND `isactive` = "Y") AS` pay_type`',
                    '`jupayment_transaction`.`source_id`',
                    '`jupayment_transaction`.`pay_code`',
                    '`jupayment_transaction`.`pay_code_desc`',
                    '(SELECT `failure_message_th` FROM ' +
                    process.env.DB_DATABASE +
                    '.`tupayment_failure` WHERE `failure_code` = `jupayment_transaction`.`pay_code`) AS `pay_code_desc_th`',
                ])
                .from(JupaymentTransactionEntity, 'jupayment_transaction')
                .leftJoin(
                    JupaymentEntity,
                    'jupayment',
                    '`jupayment`.`id` = `jupayment_transaction`.`payment_id`',
                )
                .where("`jupayment`.`is_active` = 'Y'");

            if (!Helpers.isEmpty(reference_order)) {
                await query.andWhere(
                    '`jupayment_transaction`.`reference_order` = :reference_order',
                    {
                        reference_order: reference_order,
                    },
                );
            }
            if (!Helpers.isEmpty(chrg_test)) {
                await query.andWhere(
                    '`jupayment_transaction`.`chrg_test` = :chrg_test',
                    {
                        chrg_test: chrg_test,
                    },
                );
            }
            // await query.groupBy('`jupayment`.`id`');
            // await Helpers.getRawSql(query);
            const result = await query.getRawOne();
            if (await result) {
                return result;
            } else {
                return result;
            }
        } catch (e) {
            return e.meaasge;
        }
    }

    async getDataPaymentTransactionById(@Req() request: Request) {
        const { reference_order, chrg_test, transaction_id } = request.query;
        let data = {
            message: '',
        };
        try {
            const query = await getConnection()
                .createQueryBuilder()
                .select([
                    '`jupayment_transaction`.`id`',
                    '`jupayment`.`amount`',
                    '`jupayment`.`customer_name`',
                    '`jupayment`.`customer_tel`',
                    '`jupayment`.`merchant_id`',
                    '`jupayment`.`merchant_name`',
                    '`jupayment`.`text_sms`',
                    '`jupayment`.`bitly_url`',
                    'DATE_FORMAT(`jupayment_transaction`.`pay_date`, "%Y-%m-%d %H:%i:%s") AS pay_date',
                    '`jupayment_transaction`.`payment_id`',
                    '`jupayment_transaction`.`pay_round`',
                    '`jupayment_transaction`.`brand`',
                    '`jupayment_transaction`.`card_number`',
                    '`jupayment_transaction`.`card_bank`',
                    '`jupayment_transaction`.`pay_status`',
                    '`jupayment_transaction`.`created_at`',
                    '`jupayment_transaction`.`reference_order`',
                    '`jupayment_transaction`.`chrg_test`',
                    '`jupayment_transaction`.`notify_verify`',
                    '`jupayment_transaction`.`pay_code`',
                    '`jupayment_transaction`.`pay_code_desc`',
                    '(SELECT `failure_message_th` FROM ' +
                    process.env.DB_DATABASE +
                    '.`tupayment_failure` WHERE `failure_code` = `jupayment_transaction`.`pay_code`) AS `pay_code_desc_th`',
                    '(SELECT `ref_desc` FROM ' +
                    process.env.DB_DATABASE +
                    '.`turef` WHERE `ref_type` = "PAYMENT" AND `ref_code` = "FORM_TYPE" AND `ref_value` = `jupayment_transaction`.`pay_type` AND `isactive` = "Y") AS` pay_type`',
                ])
                .from(JupaymentTransactionEntity, 'jupayment_transaction')
                .leftJoin(
                    JupaymentEntity,
                    'jupayment',
                    '`jupayment`.`id` = `jupayment_transaction`.`payment_id`',
                )
                .where("`jupayment`.`is_active` = 'Y'");
            if (!Helpers.isEmpty(transaction_id)) {
                await query.andWhere(
                    '`jupayment_transaction`.`id` = :transaction_id',
                    {
                        transaction_id: transaction_id,
                    },
                );
            }
            // await query.groupBy('`jupayment`.`id`');
            // await Helpers.getRawSql(query);
            const result = await query.getRawOne();
            if (await result) {
                return result;
            } else {
                return result;
            }
        } catch (e) {
            return e.meaasge;
        }
    }
}
