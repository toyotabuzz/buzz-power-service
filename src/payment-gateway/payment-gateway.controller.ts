import {
  Controller,
  Get,
  Post,
  Body,
  Req,
  Param,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { AppService } from 'src/app.service';
import { PaymentGatewayService } from './payment-gateway.service';
// import { PaymentGatewayExtranal } from 'src/dto/paymentGatewayExtranal.dto';
import * as Helpers from 'src/Helpers/helpers';

@Controller('payment-gateway')
export class PaymentGatewayController {
  constructor(
    private readonly paymentGatewayService: PaymentGatewayService,
    private readonly appService: AppService,
  ) {}

  @Get('/check-data-send-sms')
  async checkDataSendSms(@Req() request: Request) {
    const result = await this.paymentGatewayService.getDataSendSms(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/check-data-send-sms',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/check-data-payment')
  async checkDataPayment(@Req() request: Request) {
    const result = await this.paymentGatewayService.checkDataPayment(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/check-data-payment',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/get-data-formall')
  async getDataFormall(@Req() request: Request) {
    const result = await this.paymentGatewayService.getRefByValue(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/get-data-formall',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/get-data-payment')
  async getDataPayment(@Req() request: Request) {
    const result = await this.paymentGatewayService.getDataPayment(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/get-data-payment',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/get-round-max')
  async getRoundMax(@Req() request: Request) {
    const result = await this.paymentGatewayService.getRoundMax(request.query);

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/get-round-max',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/create-payment')
  async createJupayment(@Req() request: Request) {
    const result = await this.paymentGatewayService.createJupayment(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/create-payment',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/create-payment-transaction')
  async createJupaymentTransaction(@Req() request: Request) {
    const result = await this.paymentGatewayService.insertJupaymentTransaction(
      request.query,
    );
    // console.log(result);
    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/create-payment-transaction',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/update-payment')
  async updateJupayment(@Req() request: Request) {
    const result = await this.paymentGatewayService.updateJupayment(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/update-payment',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/create-payment-log')
  async createJupaymentLog(@Req() request: Request) {
    const result = await this.paymentGatewayService.insertJupaymentLog(
      request.query,
    );

    const apiLogServiceResult = await this.appService.saveApiServiceLog({
      method_name: 'payment-gateway/create-payment-log',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });

    return result;
  }

  @Get('/update-payment-transaction')
  async updatePaymentTransaction(@Req() request) {
    // console.log(request.query);
    const result = await this.paymentGatewayService.updatePaymentTransaction(
      request,
    );
    const apiLogResult = await this.appService.saveApiServiceLog({
      method_name: 'buzz-power/payment-gateway/update-payment-transaction',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });
    return result;
  }

  @Get('/update-payment-transaction-status')
  async updatePaymentTransactionStatus(@Req() request) {
    const result =
      await this.paymentGatewayService.updatePaymentTransactionStatus(request);
    const apiLogResult = await this.appService.saveApiServiceLog({
      method_name:
        'buzz-power/payment-gateway/update-payment-transaction-status',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });
    return result;
  }

  @Get('/get-data-payment-transaction-by-ref')
  async getDataPaymentTransactionByRef(@Req() request) {
    // console.log(request.body);
    const result =
      await this.paymentGatewayService.getDataPaymentTransactionByRef(request);
    const apiLogResult = await this.appService.saveApiServiceLog({
      method_name:
        'buzz-power/payment-gateway/get-data-payment-transaction-by-ref',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });
    return result;
  }

  @Get('/get-data-payment-transaction')
  async getDataPaymentTransactionById(@Req() request) {
    // console.log(request.body);
    const result =
      await this.paymentGatewayService.getDataPaymentTransactionById(request);
    const apiLogResult = await this.appService.saveApiServiceLog({
      method_name:
        'buzz-power/payment-gateway/get-data-payment-transaction-by-id',
      in_service: JSON.stringify(request.query),
      return_service: JSON.stringify(result),
      created_by: null,
      updated_by: null,
    });
    return result;
  }

  @Get('/send-data-charge')
  async sendDataCharge(@Req() request: Request) {}

  @Get('/send-get-data-id')
  async sendGetDataId(@Req() request: Request) {}
}
