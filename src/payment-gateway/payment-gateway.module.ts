import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from 'src/app.service';
import { JuapiServiceLogEntity } from 'src/entities/juapiservicelog.entity';
import { JupaymentEntity } from 'src/entities/jupayment.entity';
import { JupaymentLogEntity } from 'src/entities/jupaymentlog.entity';
import { JupaymentTransactionEntity } from 'src/entities/jupaymenttransaction.entity';
import { PaymentGatewayController } from './payment-gateway.controller';
import { PaymentGatewayService } from './payment-gateway.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            JuapiServiceLogEntity,
            JupaymentEntity,
            JupaymentLogEntity,
            JupaymentTransactionEntity,
        ]),
    ],
    controllers: [PaymentGatewayController],
    providers: [PaymentGatewayService, AppService]
})
export class PaymentGatewayModule {}
