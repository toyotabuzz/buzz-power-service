"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.httpGet = exports.apiSend = exports.convartDateYmd = exports.isEmpty = exports.getRawSql = exports.getformatMobileNumberToSMS = exports.getRandomString = exports.getRandomFilxdInteger = exports.dateNow = void 0;
import axios from 'axios';

export const dateNow = (): string => {
  const date = new Date();
  const dateNow =
    [
      date.getFullYear(),
      (date.getMonth() + 1).toString().padStart(2, '0'),
      date.getDate().toString().padStart(2, '0'),
    ].join('-') +
    ' ' +
    [
      date.getHours().toString().padStart(2, '0'),
      date.getMinutes().toString().padStart(2, '0'),
      date.getSeconds().toString().padStart(2, '0'),
    ].join(':');

  return dateNow;
};

export const getRandomFilxdInteger = (length): number => {
  const randomNumber = Math.floor(
    Math.pow(10, length - 1) +
      Math.random() * (Math.pow(10, length) - Math.pow(10, length - 1) - 1),
  );

  return randomNumber;
};

export const getRandomString = (length): string => {
  const randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let result: string = '';
  for (let i = 0; i < length; i++) {
    result += randomChars.charAt(
      Math.floor(Math.random() * randomChars.length),
    );
  }
  return result.toUpperCase();
};

export const getformatMobileNumberToSMS = (number_tel): string => {
  let result = number_tel
    .replace(/^\0/, '66')
    .replace(/-/g, '')
    .replace(/\s/g, '');
  return result;
};

export const getRawSql = (query) => {
  let [sql, params] = query.getQueryAndParameters();
  params.forEach((value) => {
    if (typeof value === 'string') {
      sql = sql.replace('?', `"${value}"`);
    }
    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        sql = sql.replace(
          '?',
          value
            .map((element) =>
              typeof element === 'string' ? `"${element}"` : element,
            )
            .join(','),
        );
      } else {
        sql = sql.replace('?', value);
      }
    }
    if (['number', 'boolean'].includes(typeof value)) {
      sql = sql.replace('?', value.toString());
    }
  });

  console.log(sql);
};

export const apiSend = async (
  methodApi: any,
  url: string,
  params: any = null,
) => {
  const method = methodApi.toUpperCase();
  if (method == 'GET') {
    const response = await axios({
      method,
      url,
      data: params,
    });
    const { data } = response;
    return data;
  } else if (method == 'POST') {
    const response = await axios({
      method,
      url,
      data: params,
    });
    const { data } = response;
    return data;
  }
};

export const isEmpty = (req) => {
  try {
    let boolean = false;
    if (Array.isArray(req)) {
      if (req === null || req === undefined || Object.keys(req).length === 0) {
        boolean = true;
      }
    } else {
      if (req === null || req === '' || req === undefined || req === 0) {
        boolean = true;
      }
    }

    return boolean;
  } catch (error) {
    console.log(error.message);
  }
};
